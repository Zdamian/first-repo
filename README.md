# Boilerplate app 

To start working with this app run once
`git clone git@bitbucket.org:Zdamian/first-repo.git`
next run 
`npm install`

To play with this app run
`npm start`

To watch SCSS changes run in a separate terminal tab
`gulp watch-sass`
